#!/bin/bash 

# Copyright (C) 2017 Kipp A. Aldrich

# Get the temp and humidity from /tmp/ht-history.txt last line
# if that file exists, it is update every 2 seconds or so
# it is a historical file that grows over time

output_file="/dev/shm/mjpeg/user_annotate.txt"
ht_file="/home/pi/ht-history.txt"

if [ -f "$ht_file" ] ; then
    # convert date/times to seconds since epoch for comparison
    xxdate=$(date +%s)
    # load the last line of the history file and collapse sapces to one
    lastline=$(tail -n 1 $ht_file | tr -s " ")
    htdate=$(echo "$lastline"|cut -d' ' -f 1-5)
    hhdate=$(date -d "$htdate" +%s)
    hdata=$(echo "$lastline"|cut -d' ' -f 6)
    tdata=$(echo "$lastline"|cut -d' ' -f 7)
    fldata=$(echo "$lastline"|cut -f 6)
    diffdate=$((xxdate - hhdate))

    echo lastline $lastline
    echo xxdate $xxdate
    echo htdate $htdate
    echo hhdate $hhdate
    echo hdata $hdata
    echo tdata $tdata
    echo fldata $fldata
    echo diffdate $diffdate

    # if humidity and temp data is too old, don't use them
    # if older than one minute, don't output the data
    if [ "$diffdate" -le "60" ]; then
        echo "Updating $hdata $tdata "
        echo " $hdata $tdata " > "$output_file"
    fi
fi
