#!/bin/bash
        
# $1 is the path to an executable file, for which we will
# find the location of the NEEDED libraries

if [ -z "$1" ]; then
        echo "Please provide a path to an executable"
        exit;
fi

readelf -d $1 | grep NEEDED | \
awk '{ print $5; }' | \
tr -d '[]' | \
while read -r line ; do 
    echo
    echo ">>>>> Searching for $line"
    sudo find / -not \( -path '/var' -prune -o -path '/proc' -prune -o -path '/run' -prune \) -name "$line" -print 
    echo "Searching next"
done; 
echo;
