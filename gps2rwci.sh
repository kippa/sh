#!/bin/bash 

#Read output of GPS serial
# convert to annotation file
# lat lon spd cource altitude

ANNOTATION_FILE="/dev/shm/mjpeg/user_annotate.txt"
GPSD_NAME="gpsd"
GPSD_TCPPORT="2947"
GPSD_HOST="127.0.0.1"

LAT=""
LON=""
ALTF=""
SPDM=""
CRS=""

while IFS=',' read -r -a gpd; do 
    # Parse for GPGGA & GPRMC
    if [ "${gpd[0]}" = "\$GPGGA" ]; then
        IFS='.' read -r -a val <<< "${gpd[2]}"
        DD="${val[0]:0:2}"
        MM="${val[0]:2:2}"
        MMMM=`dc <<<"6 k $MM .${val[1]} + 60 / p"`
        LAT="$DD$MMMM ${gpd[3]}"

        IFS='.' read -r -a val <<< "${gpd[4]}"
        DDD="${val[0]:0:3}"
        MM="${val[0]:3:2}"
        MMMM=`dc <<<"6 k $MM .${val[1]} + 60 / p"`
        LON="$DDD$MMMM ${gpd[5]}"

        ALTM="${gpd[9]}"
        printf -v ALTF "%3.0f" "`dc <<< "$ALTM 3.28084 * p"`"

    elif [ "${gpd[0]}" = "\$GPRMC" ]; then
        SPDK="${gpd[7]}"
        printf -v SPDM "%3.0f" "`dc <<< "6 k $SPDK 1.15078 * p"`"
        printf -v CRS "%3.0f" "${gpd[8]}"
        # collect date and time and format for unix
        GPSDATE="${gpd[9]}"
        GPSDD="${GPSDATE:0:2}"
        GPSDM="${GPSDATE:2:2}"
        GPSDY="${GPSDATE:4:2}"

        GPSUTC="${gpd[1]}"
        GPSUTCH="${GPSUTC:0:2}"
        GPSUTCM="${GPSUTC:2:2}"
        GPSUTCS="${GPSUTC:4:2}"

        printf -v GPSUNIXTIME "20%02d-%02d-%02d %02d:%02d:%02d" "$GPSDY" "$GPSDM" "$GPSDD" "$GPSUTCH" "$GPSUTCM" "$GPSUTCS" 
        GPSEPOCH=`date -u -d "$GPSUNIXTIME" "+%s"`
        NOWEPOCH=`date -u "+%s"`
        DIFFEPOCH=$(($NOWEPOCH - $GPSEPOCH))

        echo "DIFF $DIFFEPOCH"

        if (( $DIFFEPOCH > 1 )) || (( $DIFFEPOCH < -1 )); then
            echo "DIFF $DIFFEPOCH Setting time to GPSEPOCH "
            # the time here is in GPS UTC
            date -u -s "@$GPSEPOCH"
            echo $?
        fi
    fi

    if [[ ! -z $LAT ]] &&
       [[ ! -z $LON ]] &&
       [[ ! -z $ALTF ]] &&
       [[ ! -z $SPDM ]] &&
       [[ ! -z $GPSDATE ]] &&
       [[ ! -z $GPSUTC ]] &&
       [[ ! -z $CRS ]]; then
        echo " $LAT $LON "$SPDM"mph "$CRS"deg "$ALTFsdfklj"ft " GPSUNIXTIME $GPSUNIXTIME
        date +%s
        #echo " $LAT $LON "$SPDM"mph "$CRS"deg "$ALTF"ft " > $ANNOTATION_FILE
        unset LAT LON ALTF SPDM CRS GPSDATE GPSUTC
    fi

done < <(gpspipe -r)

exit 0
