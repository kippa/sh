#!/bin/bash

# convert all .wma files to .wav
for file in `ls *.WMA`; do  mplayer $file -ao pcm:file=`echo $file | cut -d. -f 1`.wav; done;
