#!/bin/bash 

# Copyright (C) 2017 Kipp A. Aldrich
# script to check on the number of connections to 
# the Rpi Cam web interface and report that number
# in the /dev/shm/mjpeg/user_annotate.txt file
# so it is displayed in the camera view

# discover the port  
config_file="/home/pi/RPi_Cam_Web_Interface/config.txt"
output_file="/dev/shm/mjpeg/user_annotate.txt"
ht_file="/home/pi/ht-history.txt"

if [ ! -f "$config_file" ] ; then
    echo "$config_file does not exist, error exit"
    exit 1
fi 

webport=`cat "$config_file" | grep webport | awk -F '"' '{ print $2 }'`

if [ -z "$webport" ]; then
    echo "No webport in $config_file, error exit"
    exit 2;
fi

# how many connections are there to the 7872 port?
result=`netstat -ant | grep -e 'tcp\b' | grep -e "$webport" | grep -e 'ESTABLISHED' | awk '{ gsub(/':'/," "); print $6 }' | sort | uniq | wc -l`
#result=`netstat -ant | grep -e 'tcp\b' | grep -e "$webport" | grep -e 'ESTABLISHED' | awk '{ gsub(/':'/," "); print $6 }' | uniq | wc -l`

echo $result

if [ "$result" == "1" ]; then
    echo " There is $result connection " > "$output_file"
else
    echo " There are $result connections " > "$output_file"
fi

# Now get the temp and humidity from /tmp/ht-history.txt last line
# if that file exists, it is update every 2 seconds or so
# it is a historical file that grows over time
if [ -f "$ht_file" ] ; then
    # convert date/times to seconds since epoch for comparison
    xxdate=$(date +%s)
    htdate=`tail -n 1 $ht_file | cut -d ' ' -f 1-5`
    hhdate=$(date -d "$htdate" +%s)
    htdata=`tail -n 1 $ht_file | cut -d ' ' -f 6-7`
    diffdate=$((xxdate - hhdate))
    # if humidity and temp data is too old, don't use them
    # if older than one minute, don't output the data
    if [ "$diffdate" -le "60" ]; then
        echo " $htdata " >> "$output_file"
    fi
fi
