#!/bin/bash

# copyright Kipp A. Aldrich 2017

# https://people.redhat.com/zaitcev/linux/OLS05_zaitcev.pdf 
# mount -t debugfs none_debugs /sys/kernel/debug
# modprobe usbmon

# We'll use some coloring on the gawk output
RED='\033[01;31m'
GREEN='\033[01;32m'
YELLOW='\033[01;33m'
BLUE='\033[01;34m'
NONE='\033[0m'

clear

# read the usbmon might have to change the bus from 3t 
# this is meant to monitor rsdk hpsdr usb interface only
#cat /sys/kernel/debug/usb/usbmon/3t | awk '/[B,C][i,o]:.*:0[0,2,4,6]/{print}'  | grep "\-71"
#awk '/[B,C][i,o]:.*:0[0,2,4,6]/{print}' < /sys/kernel/debug/usb/usbmon/3t 
/usr/bin/gawk -v n=$NONE -v r=$RED -v g=$GREEN -v b=$BLUE -v y=$YELLOW -v val=0 '/-71/ { printf r val n " " g strftime("%H:%M:%S", systime()) n " "  $0 "\n"; val++; }' /sys/kernel/debug/usb/usbmon/3t

